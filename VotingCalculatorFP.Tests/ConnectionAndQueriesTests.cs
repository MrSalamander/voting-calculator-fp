﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VotingCalculatorFP.Model.DAO;

namespace VotingCalculatorFP.Tests
{
	[TestClass]
	public class ConnectionAndQueriesTests
	{
		[TestMethod]
		public void TestUserHadAlreadyVoted()
		{
			var value = User.HadAlreadyVoted("97031405395");
			Assert.IsTrue(value);
		}
	}
}
