﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VotingCalculatorFP.Model;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Tests
{
	[TestClass]
	public class ModelTests
	{
		public ModelTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void TestLoadBlockedUsers()
		{
			PrivateObject obj = new PrivateObject(typeof(MainModel));
	
			obj.Invoke("LoadBlockedUsers");

			DisallowedObj dis = (DisallowedObj)obj.GetProperty("DisallowedObj");
			Equals(dis.Disallowed.Person[0].pesel, "73101015127");

		}

		[TestMethod]
		public void TestRefreshCandidatesFromURL()
		{
			PrivateObject obj = new PrivateObject(typeof(MainModel));

			obj.Invoke("RefreshCandidatesFromURL");
			CandidatesObj can =  (CandidatesObj)obj.GetProperty("CandidatesObj");
			Assert.IsNotNull(can);
			Equals(can.Candidates.Candidate[0].Name, "Mieszko I");
			Equals(can.Candidates.Candidate[0].Votes, 0);
		
		}

		[TestMethod]
		public void TestLoadCandidates()
		{
			PrivateObject obj = new PrivateObject(typeof(MainModel));
			var temp = (List<Candidate>)obj.Invoke("LoadCandidates");


		}



		}
}
