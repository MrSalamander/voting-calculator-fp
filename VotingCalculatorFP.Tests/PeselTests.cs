﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VotingCalculatorFP.Model;

namespace VotingCalculatorFP.Tests
{
	[TestClass]
	public class PeselTests
	{
		

		[TestMethod]
		public void IsNotOver18Test()
		{
			Assert.IsFalse(PeselValidator.IsOver18("10230381295"));
		}

		[TestMethod]
		public void IsOver18Test()
		{
			Assert.IsTrue(PeselValidator.IsOver18("85062924974"));
		}


		[TestMethod]
		public void BuildBirthDateTest()
		{
			PrivateType privateHelperType = new PrivateType(typeof(PeselValidator));
			var actual = (DateTime)privateHelperType.InvokeStatic("BuildBirthDate", "10262937312");
		}

		[TestMethod]
		public void getBirthYearTest()
		{
			PrivateType privateHelperType = new PrivateType(typeof(PeselValidator));

			string actual = privateHelperType.InvokeStatic("getBirthYear", "10262937312").ToString();
			string expected = "2010";
	
			Assert.AreEqual(expected, actual );
		}

		[TestMethod]
		public void getBirthYearTest2()
		{
			PrivateType privateHelperType = new PrivateType(typeof(PeselValidator));

			string actual = privateHelperType.InvokeStatic("getBirthYear", "00462959881").ToString();
			string expected = "2100";

			Assert.AreEqual(expected, actual);
		}


		[TestMethod]
		public void getBirthMonthTest()
		{
			PrivateType privateHelperType = new PrivateType(typeof(PeselValidator));

			string actual = privateHelperType.InvokeStatic("getBirthMonth", "10262937312").ToString();
			string expected = "06";

			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void getBirthDayTest()
		{
			PrivateType privateHelperType = new PrivateType(typeof(PeselValidator));

			string actual = privateHelperType.InvokeStatic("getBirthDay", "10262937312").ToString();
			string expected = "29";

			Assert.AreEqual(expected, actual);
		}
	}
}
