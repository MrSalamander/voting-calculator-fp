﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace VotingCalculatorFP
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			System.Windows.Forms.Application.EnableVisualStyles();
			System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
			

			Model.MainModel model = new Model.MainModel();
			Application app = new Application();
			Presenter.Presenter presenter = new Presenter.Presenter(model, app);

			System.Windows.Forms.Application.Run(app);
		}
	}
}
