﻿using VotingCalculatorFP.Model;
using VotingCalculatorFP.Model.DAO;
using VotingCalculatorFP.Viev;
using VotingCalculatorFP.Viev.Settings;

namespace VotingCalculatorFP.Presenter
{
	class Presenter
	{
		Application app;
		MainModel model;

		public Presenter(MainModel model, Application app)
		{
			this.app = app;
			this.model = model;

			ObserveApp();
			ObserveModelErrors();
		}

		private void ObserveApp()
		{
			//
			//	LoginPanel
			//
			((ILoginPanel)app.LoginPanel).IsPeselProper += PeselValidator.IsPeselProper;
			((ILoginPanel)app.LoginPanel).IsOver18 += PeselValidator.IsOver18;
			((ILoginPanel)app.LoginPanel).CanVote += model.CanVote;
			((ILoginPanel)app.LoginPanel).CreateVoter += model.CreateUser;
			//
			//	VotingPanel
			//
			((IVotingPanel)app.VotingPanel).LoadCandidatesList += model.LoadCandidates;
			((IVotingPanel)app.VotingPanel).Vote += model.Vote;
			//
			//	StatisticsPanel
			//
			((IStatisticsPanel)app.StatisticsPanel).LoadCandidatesList += model.LoadCandidates;
			((IStatisticsPanel)app.StatisticsPanel).LoadParties += model.LoadPartyList;
			((IStatisticsPanel)app.StatisticsPanel).CountInvalidVotes += model.CountInvalidVotes;
			//
			//	SettingsPanel
			//
			((ISettingsPanel)app.SettingsPanel).ApplyDatabaseSettings += DatabaseConnector.SetConnectionString;

		}

		private void ObserveModelErrors()
		{
			VotingUserRepository.SendMessageToView += app.DisplayErrorMessage;
			DatabaseConnector.SendMessageToView += app.DisplayErrorMessage;
			User.SendMessageToView += app.DisplayErrorMessage;
		}

	}
}
