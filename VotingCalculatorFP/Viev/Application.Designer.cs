﻿namespace VotingCalculatorFP
{
	partial class Application
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Application));
			this.HomePanel = new System.Windows.Forms.Panel();
			this.LoginPanel = new VotingCalculatorFP.Viev.LoginPanel();
			this.VotingPanel = new VotingCalculatorFP.Viev.VotingPanel();
			this.StatisticsPanel = new VotingCalculatorFP.Viev.StatisticsPanel();
			this.SettingsButton = new System.Windows.Forms.Button();
			this.MenuPanel = new System.Windows.Forms.Panel();
			this.SaveFileButton = new System.Windows.Forms.Button();
			this.CandidatesButton = new System.Windows.Forms.Button();
			this.VoteButton = new System.Windows.Forms.Button();
			this.StripeBarPanel = new System.Windows.Forms.Panel();
			this.SettingsPanel = new VotingCalculatorFP.Viev.Settings.SettingsPanel();
			this.HomePanel.SuspendLayout();
			this.MenuPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// HomePanel
			// 
			this.HomePanel.Controls.Add(this.SettingsPanel);
			this.HomePanel.Controls.Add(this.LoginPanel);
			this.HomePanel.Controls.Add(this.VotingPanel);
			this.HomePanel.Controls.Add(this.StatisticsPanel);
			this.HomePanel.Dock = System.Windows.Forms.DockStyle.Right;
			this.HomePanel.Location = new System.Drawing.Point(266, 0);
			this.HomePanel.Name = "HomePanel";
			this.HomePanel.Size = new System.Drawing.Size(874, 738);
			this.HomePanel.TabIndex = 0;
			this.HomePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.HomePanel_Paint);
			// 
			// LoginPanel
			// 
			this.LoginPanel.BackColor = System.Drawing.Color.Transparent;
			this.LoginPanel.Location = new System.Drawing.Point(0, 3);
			this.LoginPanel.Name = "LoginPanel";
			this.LoginPanel.Size = new System.Drawing.Size(874, 738);
			this.LoginPanel.TabIndex = 0;
			this.LoginPanel.Visible = false;
			// 
			// VotingPanel
			// 
			this.VotingPanel.BackColor = System.Drawing.Color.Transparent;
			this.VotingPanel.Location = new System.Drawing.Point(3, 3);
			this.VotingPanel.Name = "VotingPanel";
			this.VotingPanel.Size = new System.Drawing.Size(874, 738);
			this.VotingPanel.TabIndex = 1;
			this.VotingPanel.Visible = false;
			// 
			// StatisticsPanel
			// 
			this.StatisticsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StatisticsPanel.BackColor = System.Drawing.Color.Transparent;
			this.StatisticsPanel.Location = new System.Drawing.Point(0, 3);
			this.StatisticsPanel.Name = "StatisticsPanel";
			this.StatisticsPanel.Size = new System.Drawing.Size(874, 738);
			this.StatisticsPanel.TabIndex = 2;
			// 
			// SettingsButton
			// 
			this.SettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.SettingsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.SettingsButton.FlatAppearance.BorderSize = 0;
			this.SettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SettingsButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SettingsButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.SettingsButton.Image = ((System.Drawing.Image)(resources.GetObject("SettingsButton.Image")));
			this.SettingsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.SettingsButton.Location = new System.Drawing.Point(12, 332);
			this.SettingsButton.Name = "SettingsButton";
			this.SettingsButton.Size = new System.Drawing.Size(248, 77);
			this.SettingsButton.TabIndex = 2;
			this.SettingsButton.Text = "Settings";
			this.SettingsButton.UseVisualStyleBackColor = false;
			this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
			// 
			// MenuPanel
			// 
			this.MenuPanel.Controls.Add(this.SaveFileButton);
			this.MenuPanel.Controls.Add(this.CandidatesButton);
			this.MenuPanel.Controls.Add(this.VoteButton);
			this.MenuPanel.Controls.Add(this.SettingsButton);
			this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
			this.MenuPanel.Location = new System.Drawing.Point(0, 0);
			this.MenuPanel.Name = "MenuPanel";
			this.MenuPanel.Size = new System.Drawing.Size(266, 738);
			this.MenuPanel.TabIndex = 1;
			// 
			// SaveFileButton
			// 
			this.SaveFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.SaveFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.SaveFileButton.FlatAppearance.BorderSize = 0;
			this.SaveFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SaveFileButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SaveFileButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.SaveFileButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveFileButton.Image")));
			this.SaveFileButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.SaveFileButton.Location = new System.Drawing.Point(12, 249);
			this.SaveFileButton.Name = "SaveFileButton";
			this.SaveFileButton.Size = new System.Drawing.Size(248, 77);
			this.SaveFileButton.TabIndex = 5;
			this.SaveFileButton.Text = "    Save current state ";
			this.SaveFileButton.UseVisualStyleBackColor = false;
			this.SaveFileButton.Click += new System.EventHandler(this.SaveFileButton_Click);
			// 
			// CandidatesButton
			// 
			this.CandidatesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.CandidatesButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.CandidatesButton.FlatAppearance.BorderSize = 0;
			this.CandidatesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.CandidatesButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.CandidatesButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.CandidatesButton.Image = ((System.Drawing.Image)(resources.GetObject("CandidatesButton.Image")));
			this.CandidatesButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.CandidatesButton.Location = new System.Drawing.Point(12, 166);
			this.CandidatesButton.Name = "CandidatesButton";
			this.CandidatesButton.Size = new System.Drawing.Size(248, 77);
			this.CandidatesButton.TabIndex = 4;
			this.CandidatesButton.Text = "    List of candidates";
			this.CandidatesButton.UseVisualStyleBackColor = false;
			this.CandidatesButton.Click += new System.EventHandler(this.CandidatesButton_Click);
			// 
			// VoteButton
			// 
			this.VoteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.VoteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.VoteButton.FlatAppearance.BorderSize = 0;
			this.VoteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.VoteButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.VoteButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.VoteButton.Image = ((System.Drawing.Image)(resources.GetObject("VoteButton.Image")));
			this.VoteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.VoteButton.Location = new System.Drawing.Point(12, 83);
			this.VoteButton.Name = "VoteButton";
			this.VoteButton.Size = new System.Drawing.Size(248, 77);
			this.VoteButton.TabIndex = 3;
			this.VoteButton.Text = "Vote";
			this.VoteButton.UseVisualStyleBackColor = false;
			this.VoteButton.Click += new System.EventHandler(this.VoteButton_Click);
			// 
			// StripeBarPanel
			// 
			this.StripeBarPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StripeBarPanel.BackColor = System.Drawing.SystemColors.Highlight;
			this.StripeBarPanel.Location = new System.Drawing.Point(0, 0);
			this.StripeBarPanel.Name = "StripeBarPanel";
			this.StripeBarPanel.Size = new System.Drawing.Size(1140, 10);
			this.StripeBarPanel.TabIndex = 6;
			// 
			// SettingsPanel
			// 
			this.SettingsPanel.Location = new System.Drawing.Point(0, 10);
			this.SettingsPanel.Name = "SettingsPanel";
			this.SettingsPanel.Size = new System.Drawing.Size(877, 725);
			this.SettingsPanel.TabIndex = 3;
			// 
			// Application
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1140, 738);
			this.Controls.Add(this.StripeBarPanel);
			this.Controls.Add(this.MenuPanel);
			this.Controls.Add(this.HomePanel);
			this.Name = "Application";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Votes Calculator";
			this.HomePanel.ResumeLayout(false);
			this.MenuPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel HomePanel;
		private System.Windows.Forms.Button SettingsButton;
		private System.Windows.Forms.Panel MenuPanel;
		private System.Windows.Forms.Button CandidatesButton;
		private System.Windows.Forms.Button VoteButton;
		private System.Windows.Forms.Button SaveFileButton;
		private System.Windows.Forms.Panel StripeBarPanel;
		public Viev.LoginPanel LoginPanel;
		public Viev.VotingPanel VotingPanel;
		public Viev.StatisticsPanel StatisticsPanel;
		public Viev.Settings.SettingsPanel SettingsPanel;
	}
}

