﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCalculatorFP.Viev.Settings
{
	public interface ISettingsPanel
	{
		string UserID { get; set; }
		string ServerIP { get; set; }
		string DatabaseName { get; set; }
		string UserPassword { get; set; }
		uint Port { get; set; }

		event Func<string, string, string, string, uint, bool> ApplyDatabaseSettings;
	}
}
