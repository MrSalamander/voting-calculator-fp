﻿namespace VotingCalculatorFP.Viev.Settings
{
	partial class SettingsPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.ApplyThemeButton = new System.Windows.Forms.Button();
			this.DarkAndRed = new System.Windows.Forms.RadioButton();
			this.FutureProcessing = new System.Windows.Forms.RadioButton();
			this.SkyBlue = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.ChangeDBSettingsButton = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.PasswordBox = new System.Windows.Forms.TextBox();
			this.DatabaseNameBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.PortBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.IPBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.UserIDBox = new System.Windows.Forms.TextBox();
			this.ColorThemeToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.panel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.groupBox2);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(874, 738);
			this.panel1.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.ApplyThemeButton);
			this.groupBox2.Controls.Add(this.DarkAndRed);
			this.groupBox2.Controls.Add(this.FutureProcessing);
			this.groupBox2.Controls.Add(this.SkyBlue);
			this.groupBox2.Font = new System.Drawing.Font("Calibri", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.groupBox2.Location = new System.Drawing.Point(29, 440);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(817, 212);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Color Theme";
			this.ColorThemeToolTip.SetToolTip(this.groupBox2, "Im verry sorry but this functionality \r\nis currently in progress.");
			// 
			// ApplyThemeButton
			// 
			this.ApplyThemeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.ApplyThemeButton.Enabled = false;
			this.ApplyThemeButton.FlatAppearance.BorderSize = 0;
			this.ApplyThemeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ApplyThemeButton.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ApplyThemeButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.ApplyThemeButton.Location = new System.Drawing.Point(21, 117);
			this.ApplyThemeButton.Name = "ApplyThemeButton";
			this.ApplyThemeButton.Size = new System.Drawing.Size(197, 72);
			this.ApplyThemeButton.TabIndex = 11;
			this.ApplyThemeButton.Text = "Apply Theme";
			this.ColorThemeToolTip.SetToolTip(this.ApplyThemeButton, "Im verry sorry but this functionality ");
			this.ApplyThemeButton.UseVisualStyleBackColor = false;
			// 
			// DarkAndRed
			// 
			this.DarkAndRed.AutoSize = true;
			this.DarkAndRed.Enabled = false;
			this.DarkAndRed.Location = new System.Drawing.Point(569, 42);
			this.DarkAndRed.Name = "DarkAndRed";
			this.DarkAndRed.Size = new System.Drawing.Size(155, 32);
			this.DarkAndRed.TabIndex = 2;
			this.DarkAndRed.Text = "Dark And Red";
			this.DarkAndRed.UseVisualStyleBackColor = true;
			// 
			// FutureProcessing
			// 
			this.FutureProcessing.AutoSize = true;
			this.FutureProcessing.Enabled = false;
			this.FutureProcessing.Location = new System.Drawing.Point(305, 42);
			this.FutureProcessing.Name = "FutureProcessing";
			this.FutureProcessing.Size = new System.Drawing.Size(196, 32);
			this.FutureProcessing.TabIndex = 1;
			this.FutureProcessing.Text = "Future Processing";
			this.FutureProcessing.UseVisualStyleBackColor = true;
			// 
			// SkyBlue
			// 
			this.SkyBlue.AutoSize = true;
			this.SkyBlue.Checked = true;
			this.SkyBlue.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.SkyBlue.Enabled = false;
			this.SkyBlue.Location = new System.Drawing.Point(81, 42);
			this.SkyBlue.Name = "SkyBlue";
			this.SkyBlue.Size = new System.Drawing.Size(107, 32);
			this.SkyBlue.TabIndex = 0;
			this.SkyBlue.TabStop = true;
			this.SkyBlue.Text = "Sky Blue";
			this.SkyBlue.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ChangeDBSettingsButton);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.PasswordBox);
			this.groupBox1.Controls.Add(this.DatabaseNameBox);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.PortBox);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.IPBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.UserIDBox);
			this.groupBox1.Font = new System.Drawing.Font("Calibri", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.groupBox1.Location = new System.Drawing.Point(29, 71);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(817, 347);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "DataBase Settings";
			// 
			// ChangeDBSettingsButton
			// 
			this.ChangeDBSettingsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.ChangeDBSettingsButton.FlatAppearance.BorderSize = 0;
			this.ChangeDBSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ChangeDBSettingsButton.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ChangeDBSettingsButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.ChangeDBSettingsButton.Location = new System.Drawing.Point(421, 240);
			this.ChangeDBSettingsButton.Name = "ChangeDBSettingsButton";
			this.ChangeDBSettingsButton.Size = new System.Drawing.Size(290, 56);
			this.ChangeDBSettingsButton.TabIndex = 10;
			this.ChangeDBSettingsButton.Text = "Change Settings";
			this.ChangeDBSettingsButton.UseVisualStyleBackColor = false;
			this.ChangeDBSettingsButton.Click += new System.EventHandler(this.ChangeDBSettingsButton_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(96, 143);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(159, 28);
			this.label4.TabIndex = 6;
			this.label4.Text = "Database Name";
			// 
			// PasswordBox
			// 
			this.PasswordBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.PasswordBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PasswordBox.Location = new System.Drawing.Point(419, 172);
			this.PasswordBox.Name = "PasswordBox";
			this.PasswordBox.Size = new System.Drawing.Size(292, 27);
			this.PasswordBox.TabIndex = 5;
			this.PasswordBox.Text = "123456789Fp";
			this.PasswordBox.UseSystemPasswordChar = true;
			// 
			// DatabaseNameBox
			// 
			this.DatabaseNameBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.DatabaseNameBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.DatabaseNameBox.Location = new System.Drawing.Point(99, 172);
			this.DatabaseNameBox.Name = "DatabaseNameBox";
			this.DatabaseNameBox.Size = new System.Drawing.Size(273, 27);
			this.DatabaseNameBox.TabIndex = 7;
			this.DatabaseNameBox.Text = "mrsala_FPVoting";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(416, 143);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(149, 28);
			this.label3.TabIndex = 4;
			this.label3.Text = "User Password";
			// 
			// PortBox
			// 
			this.PortBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.PortBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PortBox.Location = new System.Drawing.Point(419, 75);
			this.PortBox.Name = "PortBox";
			this.PortBox.Size = new System.Drawing.Size(292, 27);
			this.PortBox.TabIndex = 3;
			this.PortBox.Text = "3306";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(96, 240);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 28);
			this.label5.TabIndex = 8;
			this.label5.Text = "User ID";
			// 
			// IPBox
			// 
			this.IPBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.IPBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.IPBox.Location = new System.Drawing.Point(99, 75);
			this.IPBox.Name = "IPBox";
			this.IPBox.Size = new System.Drawing.Size(273, 27);
			this.IPBox.TabIndex = 1;
			this.IPBox.Text = "mysql-792761.vipserv.org";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(96, 46);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 28);
			this.label1.TabIndex = 0;
			this.label1.Text = "Server IP";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(416, 46);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 28);
			this.label2.TabIndex = 2;
			this.label2.Text = "Port";
			// 
			// UserIDBox
			// 
			this.UserIDBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.UserIDBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.UserIDBox.Location = new System.Drawing.Point(99, 269);
			this.UserIDBox.Name = "UserIDBox";
			this.UserIDBox.Size = new System.Drawing.Size(273, 27);
			this.UserIDBox.TabIndex = 9;
			this.UserIDBox.Text = "mrsala_App";
			// 
			// ColorThemeToolTip
			// 
			this.ColorThemeToolTip.AutomaticDelay = 50;
			this.ColorThemeToolTip.AutoPopDelay = 5000;
			this.ColorThemeToolTip.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.ColorThemeToolTip.InitialDelay = 50;
			this.ColorThemeToolTip.ReshowDelay = 10;
			// 
			// SettingsPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.panel1);
			this.Name = "SettingsPanel";
			this.Size = new System.Drawing.Size(874, 738);
			this.panel1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox PasswordBox;
		private System.Windows.Forms.TextBox DatabaseNameBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox PortBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox IPBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox UserIDBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton DarkAndRed;
		private System.Windows.Forms.RadioButton FutureProcessing;
		private System.Windows.Forms.RadioButton SkyBlue;
		private System.Windows.Forms.Button ChangeDBSettingsButton;
		private System.Windows.Forms.Button ApplyThemeButton;
		private System.Windows.Forms.ToolTip ColorThemeToolTip;
	}
}
