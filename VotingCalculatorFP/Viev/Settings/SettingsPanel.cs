﻿using System;
using System.Windows.Forms;

namespace VotingCalculatorFP.Viev.Settings
{
	public partial class SettingsPanel : UserControl, ISettingsPanel
	{
		#region Properties
		public string UserID { get { return UserIDBox.Text; } set { UserIDBox.Text = value; } }
		public string ServerIP { get { return IPBox.Text; } set { IPBox.Text = value; } }
		public string DatabaseName { get { return DatabaseNameBox.Text; } set { DatabaseNameBox.Text = value; } }
		public string UserPassword { get { return PasswordBox.Text; } set { PasswordBox.Text = value; } }
		public uint Port { get { return uint.Parse(PortBox.Text); } set { PortBox.Text = value.ToString(); } }
		#endregion

		#region Constructor
		public SettingsPanel()
		{
			InitializeComponent();
		}
		#endregion


		#region Events
		public event Func<string, string, string, string, uint, bool> ApplyDatabaseSettings;
		#endregion

		private void ChangeDBSettingsButton_Click(object sender, EventArgs e)
		{
			if(ApplyDatabaseSettings != null
				&&
			ApplyDatabaseSettings.Invoke(ServerIP, DatabaseName, UserID, UserPassword, Port))
			{
				MessageBox.Show("Settings Changed Succesfully!");
			}
			else
			{
				MessageBox.Show("Settings Changed Failed!");
			}
		}
	}
}
