﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace VotingCalculatorFP.Viev
{
	public partial class LoginPanel : UserControl, ILoginPanel
	{
		#region Properties
		public string UsersName { get { return NameBox.Text; } }
		public string UsersSurname { get { return SurnameBox.Text; } }
		public string UsersPESEL { get { return PeselBox.Text; } }
		#endregion

		#region Constructors
		public LoginPanel()
		{
			InitializeComponent();
		}
		#endregion

		#region Events
		public event Func<string, bool> IsPeselProper;
		public event Func<string, string, string, bool> CanVote;
		public event Action<string, string, string> CreateVoter;
		public event Action GoToVotePanel;
		public event Func<string, bool> IsOver18;
		#endregion

		#region EventHandling
		private void SignInButton_Click(object sender, EventArgs e)
		{

			if (!IsPeselCorrect())
			{
				MessageBox.Show("PESEL number is not proper","", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				ClearPanel();
				return;
			}
				
			if(!UserIsOver18())
			{
				MessageBox.Show("You are not an adult","", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				ClearPanel();
				return;
			}

			if (CanVote != null && CanVote(UsersName, UsersSurname, UsersPESEL))
			{
				if (NameAndSurnameAreCorrect())
				{
					CreateVoter?.Invoke(UsersName, UsersSurname, UsersPESEL);
					ClearPanel();
					GoToVotePanel?.Invoke();
					MessageBox.Show("You Can vote for only ONE candidate \nOther options will be treated as invalid.\n","Tip",MessageBoxButtons.OK,MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Fields cannot be blank or starts with small letter.","Wrong credentials", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					ClearPanel();
				}
			}
			else
			{
				MessageBox.Show("You are not allowed to vote","",MessageBoxButtons.OK, MessageBoxIcon.Warning);
				ClearPanel();
				return;
			}
		}
		#endregion

		#region Low abstraction level methods
		public void ClearPanel()
		{
			NameBox.Text = string.Empty;
			SurnameBox.Text = string.Empty;
			PeselBox.Text = string.Empty;
		}
		private bool NameAndSurnameAreCorrect()
		{

			if (UsersName != string.Empty && UsersSurname != string.Empty)
				if (char.IsUpper(UsersName[0]) && char.IsUpper(UsersSurname[0]))
				{
					return true;
				}
			return false;
				
		}

		private bool IsPeselCorrect()
		{
			if (IsPeselProper == null) return false;
			
			if ( !IsPeselProper(UsersPESEL) )
			{
				
				return false;
			}
			else return true;
		}

		private bool UserIsOver18()
		{
			if (IsOver18 == null) return false;

			if (!IsOver18(UsersPESEL))
			{
				return false;
			}
			else return true;
		}

	
		#endregion
	}
}
