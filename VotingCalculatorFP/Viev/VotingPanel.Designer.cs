﻿namespace VotingCalculatorFP.Viev
{
    partial class VotingPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VotingPanel));
			this.BackgroundPanel = new System.Windows.Forms.Panel();
			this.LogoutButton = new System.Windows.Forms.Button();
			this.VoteButton = new System.Windows.Forms.Button();
			this.ScrollGroupBox = new System.Windows.Forms.GroupBox();
			this.ScrollPanel = new System.Windows.Forms.Panel();
			this.BackgroundPanel.SuspendLayout();
			this.ScrollGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// BackgroundPanel
			// 
			this.BackgroundPanel.BackColor = System.Drawing.Color.Transparent;
			this.BackgroundPanel.Controls.Add(this.LogoutButton);
			this.BackgroundPanel.Controls.Add(this.VoteButton);
			this.BackgroundPanel.Controls.Add(this.ScrollGroupBox);
			this.BackgroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BackgroundPanel.Location = new System.Drawing.Point(0, 0);
			this.BackgroundPanel.Name = "BackgroundPanel";
			this.BackgroundPanel.Size = new System.Drawing.Size(874, 738);
			this.BackgroundPanel.TabIndex = 0;
			// 
			// LogoutButton
			// 
			this.LogoutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.LogoutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.LogoutButton.FlatAppearance.BorderSize = 0;
			this.LogoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.LogoutButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LogoutButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.LogoutButton.Image = ((System.Drawing.Image)(resources.GetObject("LogoutButton.Image")));
			this.LogoutButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.LogoutButton.Location = new System.Drawing.Point(70, 637);
			this.LogoutButton.Name = "LogoutButton";
			this.LogoutButton.Size = new System.Drawing.Size(263, 77);
			this.LogoutButton.TabIndex = 7;
			this.LogoutButton.Text = "    Log Out";
			this.LogoutButton.UseVisualStyleBackColor = false;
			this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
			// 
			// VoteButton
			// 
			this.VoteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.VoteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.VoteButton.FlatAppearance.BorderSize = 0;
			this.VoteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.VoteButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.VoteButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.VoteButton.Image = ((System.Drawing.Image)(resources.GetObject("VoteButton.Image")));
			this.VoteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.VoteButton.Location = new System.Drawing.Point(548, 637);
			this.VoteButton.Name = "VoteButton";
			this.VoteButton.Size = new System.Drawing.Size(263, 77);
			this.VoteButton.TabIndex = 4;
			this.VoteButton.Text = "Vote";
			this.VoteButton.UseVisualStyleBackColor = false;
			this.VoteButton.Click += new System.EventHandler(this.VoteButton_Click);
			// 
			// ScrollGroupBox
			// 
			this.ScrollGroupBox.AutoSize = true;
			this.ScrollGroupBox.BackColor = System.Drawing.Color.Transparent;
			this.ScrollGroupBox.Controls.Add(this.ScrollPanel);
			this.ScrollGroupBox.Location = new System.Drawing.Point(64, 14);
			this.ScrollGroupBox.Name = "ScrollGroupBox";
			this.ScrollGroupBox.Size = new System.Drawing.Size(753, 602);
			this.ScrollGroupBox.TabIndex = 0;
			this.ScrollGroupBox.TabStop = false;
			// 
			// ScrollPanel
			// 
			this.ScrollPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ScrollPanel.AutoScroll = true;
			this.ScrollPanel.Location = new System.Drawing.Point(6, 19);
			this.ScrollPanel.Name = "ScrollPanel";
			this.ScrollPanel.Size = new System.Drawing.Size(741, 564);
			this.ScrollPanel.TabIndex = 0;
			// 
			// VotingPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.BackgroundPanel);
			this.Name = "VotingPanel";
			this.Size = new System.Drawing.Size(874, 738);
			this.BackgroundPanel.ResumeLayout(false);
			this.BackgroundPanel.PerformLayout();
			this.ScrollGroupBox.ResumeLayout(false);
			this.ResumeLayout(false);

        }

		#endregion

		private System.Windows.Forms.Panel BackgroundPanel;
		private System.Windows.Forms.GroupBox ScrollGroupBox;
		private System.Windows.Forms.Panel ScrollPanel;
		private System.Windows.Forms.Button VoteButton;
		private System.Windows.Forms.Button LogoutButton;
	}
}
