﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Viev
{
	public partial class StatisticsPanel : UserControl, IStatisticsPanel
	{
		#region Properties
		public List<Candidate> Candidates = new List<Candidate>();
		public List<Party> Parties = new List<Party>();
		#endregion

		#region Events
		public event Action GoBackToHomePanel;
		public event Func<List<Candidate>> LoadCandidatesList;
		public event Func<uint> CountInvalidVotes;
		public event Func<List<Party>> LoadParties;
		#endregion

		#region Constructor
		public StatisticsPanel()
		{
			InitializeComponent();
		}

		#endregion
		public void LoadPartiesToGridViev()
		{
			RefreshParties();
			PartyGridViev.Rows.Clear();
			int n = 0;
			if (Parties == null)
			{
				return;
			}
			foreach (Party p in Parties)
			{
				n = PartyGridViev.Rows.Add();
				PartyGridViev.Rows[n].Cells[0].Value = Parties[n].Name;
				PartyGridViev.Rows[n].Cells[1].Value = Parties[n].Votes;
			}
			PartyGridViev.Refresh();
		}

		public void LoadCandidatesToGridViev()
		{
			RefreshCandidates();
			CandidateGridViev.Rows.Clear();
			int n = 0;
			if(Candidates == null)
			{
				InvalidVotesLabel.Text = "Invalid Votes:  ";
				return;
			}
			foreach(Candidate c in Candidates)
			{
				n = CandidateGridViev.Rows.Add();
				CandidateGridViev.Rows[n].Cells[0].Value = Candidates[n].Name;
				CandidateGridViev.Rows[n].Cells[1].Value = Candidates[n].Party;
				CandidateGridViev.Rows[n].Cells[2].Value = Candidates[n].Votes;
			}
			CandidateGridViev.Refresh();

			InvalidVotesLabel.Text = "Invalid Votes:  " + CountInvalidVotes?.Invoke();
		}

		public void LoadCandidatesChart()
		{
			CandidatesChart.Series["Votes"].Points.Clear();
			if (Candidates == null)
			{
				return;
			}
			foreach (Candidate c in Candidates)
			{
				this.CandidatesChart.Series["Votes"].Points.AddXY(c.Name, c.Votes);
			}
		}

		public void LoadPartysChart()
		{
			PartiesChart.Series["Votes"].Points.Clear();
			if (Parties == null) return;
			foreach (Party c in Parties)
			{
				this.PartiesChart.Series["Votes"].Points.AddXY(c.Name, c.Votes);
			}
		}

		private void RefreshCandidates()
		{
			if (LoadCandidatesList != null) Candidates = LoadCandidatesList.Invoke();
		}

		private void RefreshParties()
		{
			if (LoadParties != null) Parties = LoadParties.Invoke();
		}

		#region Event Handling
		private void LogoutButton_Click(object sender, EventArgs e)
		{
			GoBackToHomePanel?.Invoke();
		}

		private void PartiesButton_Click(object sender, EventArgs e)
		{
			CandidatesVotesPanel.Visible = false;
			ChartPanel.Visible = false;
			PartiesVotesPanel.Visible = true;
		}

		private void CandidatesButton_Click(object sender, EventArgs e)
		{
			CandidatesVotesPanel.Visible = true;
			ChartPanel.Visible = false;
			PartiesVotesPanel.Visible = false;
		}

		private void ChartButton_Click(object sender, EventArgs e)
		{
			CandidatesVotesPanel.Visible = false;
			ChartPanel.Visible = true;
			PartiesVotesPanel.Visible = false;
		}
		#endregion

		private void StatisticsPanel_Load(object sender, EventArgs e)
		{

		}
	}
}
