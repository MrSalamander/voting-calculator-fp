﻿using System.ComponentModel;
using System.Windows.Forms;

namespace VotingCalculatorFP.Viev
{
	[Browsable(false)]
	public partial class CandidateControl : UserControl
	{
		#region Properties
		public string CandidateName { get { return NameLabel.Text; } set { NameLabel.Text = value; } }
		public string Party { get { return PartyLabel.Text; } set { PartyLabel.Text = value; } }
		public bool Checked { get { return CheckBox.Checked; } }
		#endregion

		public CandidateControl()
		{
			InitializeComponent();
		}

		public CandidateControl(string name, string party)
		{
			InitializeComponent();
			CandidateName = name;
			Party = party;
		}
	}
}
