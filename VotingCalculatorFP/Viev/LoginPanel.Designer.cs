﻿using System.Drawing;

namespace VotingCalculatorFP.Viev
{
	partial class LoginPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.GradientPanel = new System.Windows.Forms.Panel();
			this.PeselLabel = new System.Windows.Forms.Label();
			this.SurnameLabel = new System.Windows.Forms.Label();
			this.NameLabel = new System.Windows.Forms.Label();
			this.PeselBox = new System.Windows.Forms.TextBox();
			this.SurnameBox = new System.Windows.Forms.TextBox();
			this.NameBox = new System.Windows.Forms.TextBox();
			this.SignInButton = new System.Windows.Forms.Button();
			this.GradientPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// GradientPanel
			// 
			this.GradientPanel.BackColor = System.Drawing.Color.Transparent;
			this.GradientPanel.Controls.Add(this.PeselLabel);
			this.GradientPanel.Controls.Add(this.SurnameLabel);
			this.GradientPanel.Controls.Add(this.NameLabel);
			this.GradientPanel.Controls.Add(this.PeselBox);
			this.GradientPanel.Controls.Add(this.SurnameBox);
			this.GradientPanel.Controls.Add(this.NameBox);
			this.GradientPanel.Controls.Add(this.SignInButton);
			this.GradientPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GradientPanel.Location = new System.Drawing.Point(0, 0);
			this.GradientPanel.Name = "GradientPanel";
			this.GradientPanel.Size = new System.Drawing.Size(874, 738);
			this.GradientPanel.TabIndex = 0;
			// 
			// PeselLabel
			// 
			this.PeselLabel.AutoSize = true;
			this.PeselLabel.BackColor = System.Drawing.Color.Transparent;
			this.PeselLabel.Font = new System.Drawing.Font("Calibri", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PeselLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
			this.PeselLabel.Location = new System.Drawing.Point(83, 258);
			this.PeselLabel.Name = "PeselLabel";
			this.PeselLabel.Size = new System.Drawing.Size(73, 28);
			this.PeselLabel.TabIndex = 6;
			this.PeselLabel.Text = "PESEL:";
			// 
			// SurnameLabel
			// 
			this.SurnameLabel.AutoSize = true;
			this.SurnameLabel.BackColor = System.Drawing.Color.Transparent;
			this.SurnameLabel.Font = new System.Drawing.Font("Calibri", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SurnameLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
			this.SurnameLabel.Location = new System.Drawing.Point(83, 166);
			this.SurnameLabel.Name = "SurnameLabel";
			this.SurnameLabel.Size = new System.Drawing.Size(101, 28);
			this.SurnameLabel.TabIndex = 5;
			this.SurnameLabel.Text = "Surname:";
			// 
			// NameLabel
			// 
			this.NameLabel.AutoSize = true;
			this.NameLabel.BackColor = System.Drawing.Color.Transparent;
			this.NameLabel.Font = new System.Drawing.Font("Calibri", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.NameLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
			this.NameLabel.Location = new System.Drawing.Point(80, 75);
			this.NameLabel.Name = "NameLabel";
			this.NameLabel.Size = new System.Drawing.Size(73, 28);
			this.NameLabel.TabIndex = 4;
			this.NameLabel.Text = "Name:";
			// 
			// PeselBox
			// 
			this.PeselBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.PeselBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PeselBox.Font = new System.Drawing.Font("Calibri", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PeselBox.ForeColor = System.Drawing.Color.White;
			this.PeselBox.Location = new System.Drawing.Point(83, 289);
			this.PeselBox.Name = "PeselBox";
			this.PeselBox.Size = new System.Drawing.Size(617, 38);
			this.PeselBox.TabIndex = 3;
			// 
			// SurnameBox
			// 
			this.SurnameBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.SurnameBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.SurnameBox.Font = new System.Drawing.Font("Calibri", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SurnameBox.ForeColor = System.Drawing.Color.White;
			this.SurnameBox.Location = new System.Drawing.Point(83, 197);
			this.SurnameBox.Name = "SurnameBox";
			this.SurnameBox.Size = new System.Drawing.Size(617, 38);
			this.SurnameBox.TabIndex = 2;
			// 
			// NameBox
			// 
			this.NameBox.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.NameBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.NameBox.Font = new System.Drawing.Font("Calibri", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.NameBox.ForeColor = System.Drawing.Color.White;
			this.NameBox.Location = new System.Drawing.Point(83, 106);
			this.NameBox.Name = "NameBox";
			this.NameBox.Size = new System.Drawing.Size(617, 38);
			this.NameBox.TabIndex = 1;
			// 
			// SignInButton
			// 
			this.SignInButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.SignInButton.FlatAppearance.BorderSize = 0;
			this.SignInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SignInButton.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SignInButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.SignInButton.Location = new System.Drawing.Point(83, 370);
			this.SignInButton.Name = "SignInButton";
			this.SignInButton.Size = new System.Drawing.Size(617, 96);
			this.SignInButton.TabIndex = 0;
			this.SignInButton.Text = "Confirm";
			this.SignInButton.UseVisualStyleBackColor = false;
			this.SignInButton.Click += new System.EventHandler(this.SignInButton_Click);
			// 
			// LoginPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.GradientPanel);
			this.Name = "LoginPanel";
			this.Size = new System.Drawing.Size(874, 738);
			this.GradientPanel.ResumeLayout(false);
			this.GradientPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel GradientPanel;
		private System.Windows.Forms.TextBox PeselBox;
		private System.Windows.Forms.TextBox SurnameBox;
		private System.Windows.Forms.TextBox NameBox;
		private System.Windows.Forms.Button SignInButton;
		private System.Windows.Forms.Label PeselLabel;
		private System.Windows.Forms.Label SurnameLabel;
		private System.Windows.Forms.Label NameLabel;
	}
}
