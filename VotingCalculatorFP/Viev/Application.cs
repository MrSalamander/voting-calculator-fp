﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using VotingCalculatorFP.Model.DataStructs;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text;

namespace VotingCalculatorFP
{
	public partial class Application : Form
	{
		public static Color COLOR_TOP = Color.FromArgb(192, 255, 255);
		public static Color COLOR_BOTTOM = SystemColors.Control;

		#region Properties
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public List<UserControl> PanelList = new List<UserControl>();

		#endregion

		#region Constructor
		public Application()
		{
			InitializeComponent();
			ObservePanels();
			HideAllPanels();
		}
		#endregion

		public void DisplayErrorMessage(string message)
		{
			MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		private void ObservePanels()
		{
			//
			//	LoginPanel
			//
			LoginPanel.GoToVotePanel += GoToVotingPanel;
			LoginPanel.GoToVotePanel += VotingPanel.GenerateCandidatesControls;
			//
			//	VotingPanel
			//
			VotingPanel.GoBackToHomePanel += GoBackToHomePanel;
			VotingPanel.GoToSummaryPanel += GoToSummaryPanel;
			//
			//	StatisticsPanel
			//
			StatisticsPanel.GoBackToHomePanel += HideAllPanels;
		}

		#region Navigation Methods
		private void GoToSummaryPanel()
		{
			HideAllPanels();
			StatisticsPanel.LoadCandidatesToGridViev();
			StatisticsPanel.LoadPartiesToGridViev();
			StatisticsPanel.LoadCandidatesChart();
			StatisticsPanel.LoadPartysChart();
			StatisticsPanel.Visible = true;
		}

		private void HideAllPanels()
		{
			LoginPanel.Visible = false;
			VotingPanel.Visible = false;
			StatisticsPanel.Visible = false;
			SettingsPanel.Visible = false;
		}

		private void GoToVotingPanel()
		{
			HideAllPanels();
			VotingPanel.Visible = true;
		}

		private void GoToLoginPanel()
		{
			HideAllPanels();
			LoginPanel.Visible = true;
			LoginPanel.ClearPanel();
		}

		private void GoBackToHomePanel()

		{
			HideAllPanels();
			LoginPanel.ClearPanel();
		}

		private void GoToSettingsPanel()
		{
			HideAllPanels();
			SettingsPanel.Visible = true;
		}
		#endregion

		#region EventHandling
		private void VoteButton_Click(object sender, EventArgs e)
		{
			GoToLoginPanel();
		}

		private void HomePanel_Paint(object sender, PaintEventArgs e)
		{
			LinearGradientBrush lgb = new LinearGradientBrush(ClientRectangle, COLOR_BOTTOM, COLOR_TOP, -5F);
			Graphics g = e.Graphics;
			g.FillRectangle(lgb, this.ClientRectangle);
		}

		private void CandidatesButton_Click(object sender, EventArgs e)
		{
			GoToSummaryPanel();
		}

		private void SettingsButton_Click(object sender, EventArgs e)
		{
			GoToSettingsPanel();
		}

		private void SaveFileButton_Click(object sender, EventArgs e)
		{
			using (var fbd = new FolderBrowserDialog())
			{
				DialogResult result = fbd.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
				{
					GenerateAndWritePDF(fbd.SelectedPath);
					GenerateAndWriteCSV(fbd.SelectedPath);
				}
			}
		}
		#endregion

		#region Monster Methods (with iTextSharp)
		private void GenerateAndWriteCSV(string path)
		{
			try
			{
				var csv = new StringBuilder();
				foreach (Candidate c in this.StatisticsPanel.Candidates)
				{
					var Line = string.Format("{0},{1},{2}", c.Name, c.Party, c.Votes.ToString());
					csv.AppendLine(Line);
				}
				csv.AppendLine();
				foreach (Party c in this.StatisticsPanel.Parties)
				{
					var Line = string.Format("{0},{1}", c.Name, c.Votes.ToString());
					csv.AppendLine(Line);
				}

				File.WriteAllText(path + "/VotingStatement.csv", csv.ToString(), Encoding.Default);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void GenerateAndWritePDF(string path)
		{
			try
			{
				Document document = new Document(iTextSharp.text.PageSize.A4, 10, 10, 40, 40);
				PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(path + "/VotingStatement.pdf", FileMode.Create));
				document.Open();

				document.AddAuthor("Aleksander Fedyczek");
				document.AddTitle("Voting Statement");

				document.Add(new Paragraph(" Voting Statement ")
				{
					Alignment = Element.ALIGN_CENTER,
					Font = FontFactory.GetFont("Calibri", 40, iTextSharp.text.Font.BOLD)
				});
				document.Add(new Paragraph(" by Aleksander Fedyczek ")
				{
					Alignment = Element.ALIGN_CENTER,
					Font = FontFactory.GetFont("Arial", BaseFont.CP1250)	
				});
				// 
				// Generate Table 1
				//
				this.StatisticsPanel.LoadCandidatesToGridViev();
				PdfPTable CandidateVotesTable = new PdfPTable(3);
				PdfPCell cell = new PdfPCell(new Phrase("Concrete Candidates Statement"));
				cell.Colspan = 3;
				cell.HorizontalAlignment = 1;
				CandidateVotesTable.AddCell(cell);
				foreach (Candidate c in this.StatisticsPanel.Candidates)
				{
					CandidateVotesTable.AddCell(c.Name);
					CandidateVotesTable.AddCell(c.Party);
					CandidateVotesTable.AddCell(c.Votes.ToString());
				}
				document.Add(CandidateVotesTable);
				//
				// Generate Chart 1
				//
				document.NewPage();
				var ChartImage1 = new MemoryStream();
				this.StatisticsPanel.LoadCandidatesChart();
				this.StatisticsPanel.CandidatesChart.SaveImage(ChartImage1, ChartImageFormat.Png);
				iTextSharp.text.Image Chart1_image = iTextSharp.text.Image.GetInstance(ChartImage1.GetBuffer());
				Chart1_image.SetAbsolutePosition((PageSize.A4.Width - Chart1_image.ScaledWidth) / 2, (PageSize.A4.Height - Chart1_image.ScaledHeight) / 2 - 40);
				document.Add(Chart1_image);
				// 
				// Generate Table 2
				//
				document.NewPage();
				StatisticsPanel.LoadPartiesToGridViev();
				PdfPTable PartiesVotesTable = new PdfPTable(2);
				PdfPCell cell2 = new PdfPCell(new Phrase("Parties Statement"));
				cell2.Colspan = 2;
				cell2.HorizontalAlignment = 1;
				PartiesVotesTable.AddCell(cell2);
				foreach (Party c in this.StatisticsPanel.Parties)
				{
					PartiesVotesTable.AddCell(c.Name);
					PartiesVotesTable.AddCell(c.Votes.ToString());
				}
				document.Add(PartiesVotesTable);
				//
				// Generate Chart 2
				//
				var ChartImage2 = new MemoryStream();
				this.StatisticsPanel.LoadPartysChart();
				this.StatisticsPanel.PartiesChart.SaveImage(ChartImage2, ChartImageFormat.Png);
				iTextSharp.text.Image Chart2_image = iTextSharp.text.Image.GetInstance(ChartImage2.GetBuffer());
				Chart2_image.SetAbsolutePosition((PageSize.A4.Width - Chart1_image.ScaledWidth) / 2, (PageSize.A4.Height - Chart1_image.ScaledHeight) / 2 - 40);
				document.Add(Chart2_image);
				//
				// Close Document
				//
				document.Close();
				MessageBox.Show("PDF Statement Created Sucessfully");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		#endregion

	}
}
