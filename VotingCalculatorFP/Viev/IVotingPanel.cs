﻿using System;
using System.Collections.Generic;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Viev
{
	interface IVotingPanel
	{
		event Func<List<Candidate>> LoadCandidatesList;
		event Action<bool, Candidate> Vote;
		event Action GoToSummaryPanel;
		event Action GoBackToHomePanel;

	}
}
