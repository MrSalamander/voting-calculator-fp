﻿namespace VotingCalculatorFP.Viev
{
	partial class StatisticsPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatisticsPanel));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.CandidatesVotesPanel = new System.Windows.Forms.Panel();
			this.InvalidVotesLabel = new System.Windows.Forms.Label();
			this.CandidateGridViev = new System.Windows.Forms.DataGridView();
			this.CandidateName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Party = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Votes = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TopLabel = new System.Windows.Forms.Label();
			this.ChartButton = new System.Windows.Forms.Button();
			this.LogoutButton = new System.Windows.Forms.Button();
			this.CandidatesButton = new System.Windows.Forms.Button();
			this.PartiesButton = new System.Windows.Forms.Button();
			this.PartiesVotesPanel = new System.Windows.Forms.Panel();
			this.PartyGridViev = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ChartPanel = new System.Windows.Forms.Panel();
			this.PartiesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.CandidatesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.CandidatesVotesPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CandidateGridViev)).BeginInit();
			this.PartiesVotesPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PartyGridViev)).BeginInit();
			this.ChartPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PartiesChart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CandidatesChart)).BeginInit();
			this.SuspendLayout();
			// 
			// CandidatesVotesPanel
			// 
			this.CandidatesVotesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CandidatesVotesPanel.Controls.Add(this.InvalidVotesLabel);
			this.CandidatesVotesPanel.Controls.Add(this.CandidateGridViev);
			this.CandidatesVotesPanel.Location = new System.Drawing.Point(0, 0);
			this.CandidatesVotesPanel.Name = "CandidatesVotesPanel";
			this.CandidatesVotesPanel.Size = new System.Drawing.Size(843, 450);
			this.CandidatesVotesPanel.TabIndex = 0;
			// 
			// InvalidVotesLabel
			// 
			this.InvalidVotesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.InvalidVotesLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			this.InvalidVotesLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.InvalidVotesLabel.Font = new System.Drawing.Font("Calibri", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.InvalidVotesLabel.ForeColor = System.Drawing.Color.White;
			this.InvalidVotesLabel.Location = new System.Drawing.Point(0, 409);
			this.InvalidVotesLabel.Name = "InvalidVotesLabel";
			this.InvalidVotesLabel.Size = new System.Drawing.Size(843, 41);
			this.InvalidVotesLabel.TabIndex = 2;
			this.InvalidVotesLabel.Text = "Invalid votes : ";
			this.InvalidVotesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// CandidateGridViev
			// 
			this.CandidateGridViev.AllowUserToAddRows = false;
			this.CandidateGridViev.AllowUserToDeleteRows = false;
			this.CandidateGridViev.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CandidateGridViev.BorderStyle = System.Windows.Forms.BorderStyle.None;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.CandidateGridViev.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.CandidateGridViev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.CandidateGridViev.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CandidateName,
            this.Party,
            this.Votes});
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.CandidateGridViev.DefaultCellStyle = dataGridViewCellStyle5;
			this.CandidateGridViev.GridColor = System.Drawing.SystemColors.Control;
			this.CandidateGridViev.Location = new System.Drawing.Point(0, 48);
			this.CandidateGridViev.Name = "CandidateGridViev";
			this.CandidateGridViev.ReadOnly = true;
			this.CandidateGridViev.RowTemplate.Height = 50;
			this.CandidateGridViev.Size = new System.Drawing.Size(843, 358);
			this.CandidateGridViev.TabIndex = 0;
			// 
			// CandidateName
			// 
			this.CandidateName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
			this.CandidateName.DefaultCellStyle = dataGridViewCellStyle2;
			this.CandidateName.FillWeight = 99.61929F;
			this.CandidateName.HeaderText = "Candidate\'s Name";
			this.CandidateName.Name = "CandidateName";
			this.CandidateName.ReadOnly = true;
			// 
			// Party
			// 
			this.Party.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
			this.Party.DefaultCellStyle = dataGridViewCellStyle3;
			this.Party.FillWeight = 99.61929F;
			this.Party.HeaderText = "Party";
			this.Party.Name = "Party";
			this.Party.ReadOnly = true;
			// 
			// Votes
			// 
			this.Votes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
			this.Votes.DefaultCellStyle = dataGridViewCellStyle4;
			this.Votes.FillWeight = 50.76142F;
			this.Votes.HeaderText = "Votes";
			this.Votes.Name = "Votes";
			this.Votes.ReadOnly = true;
			// 
			// TopLabel
			// 
			this.TopLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TopLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.TopLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.TopLabel.Font = new System.Drawing.Font("Calibri", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.TopLabel.ForeColor = System.Drawing.Color.White;
			this.TopLabel.Location = new System.Drawing.Point(0, 0);
			this.TopLabel.Name = "TopLabel";
			this.TopLabel.Size = new System.Drawing.Size(843, 54);
			this.TopLabel.TabIndex = 1;
			this.TopLabel.Text = "Voting Statistics";
			this.TopLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ChartButton
			// 
			this.ChartButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ChartButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.ChartButton.FlatAppearance.BorderSize = 0;
			this.ChartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ChartButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ChartButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.ChartButton.Image = ((System.Drawing.Image)(resources.GetObject("ChartButton.Image")));
			this.ChartButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ChartButton.Location = new System.Drawing.Point(336, 466);
			this.ChartButton.Name = "ChartButton";
			this.ChartButton.Size = new System.Drawing.Size(165, 77);
			this.ChartButton.TabIndex = 8;
			this.ChartButton.Text = "    Chart";
			this.ChartButton.UseVisualStyleBackColor = false;
			this.ChartButton.Click += new System.EventHandler(this.ChartButton_Click);
			// 
			// LogoutButton
			// 
			this.LogoutButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.LogoutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.LogoutButton.FlatAppearance.BorderSize = 0;
			this.LogoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.LogoutButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LogoutButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.LogoutButton.Image = ((System.Drawing.Image)(resources.GetObject("LogoutButton.Image")));
			this.LogoutButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.LogoutButton.Location = new System.Drawing.Point(7, 466);
			this.LogoutButton.Name = "LogoutButton";
			this.LogoutButton.Size = new System.Drawing.Size(219, 77);
			this.LogoutButton.TabIndex = 7;
			this.LogoutButton.Text = "    Back to Home";
			this.LogoutButton.UseVisualStyleBackColor = false;
			this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
			// 
			// CandidatesButton
			// 
			this.CandidatesButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.CandidatesButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.CandidatesButton.FlatAppearance.BorderSize = 0;
			this.CandidatesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.CandidatesButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.CandidatesButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.CandidatesButton.Image = ((System.Drawing.Image)(resources.GetObject("CandidatesButton.Image")));
			this.CandidatesButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.CandidatesButton.Location = new System.Drawing.Point(507, 466);
			this.CandidatesButton.Name = "CandidatesButton";
			this.CandidatesButton.Size = new System.Drawing.Size(165, 77);
			this.CandidatesButton.TabIndex = 9;
			this.CandidatesButton.Text = "    Candidates";
			this.CandidatesButton.UseVisualStyleBackColor = false;
			this.CandidatesButton.Click += new System.EventHandler(this.CandidatesButton_Click);
			// 
			// PartiesButton
			// 
			this.PartiesButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.PartiesButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(224)))));
			this.PartiesButton.FlatAppearance.BorderSize = 0;
			this.PartiesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.PartiesButton.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PartiesButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.PartiesButton.Image = ((System.Drawing.Image)(resources.GetObject("PartiesButton.Image")));
			this.PartiesButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.PartiesButton.Location = new System.Drawing.Point(678, 466);
			this.PartiesButton.Name = "PartiesButton";
			this.PartiesButton.Size = new System.Drawing.Size(165, 77);
			this.PartiesButton.TabIndex = 10;
			this.PartiesButton.Text = "    Parties";
			this.PartiesButton.UseVisualStyleBackColor = false;
			this.PartiesButton.Click += new System.EventHandler(this.PartiesButton_Click);
			// 
			// PartiesVotesPanel
			// 
			this.PartiesVotesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PartiesVotesPanel.Controls.Add(this.PartyGridViev);
			this.PartiesVotesPanel.Location = new System.Drawing.Point(0, 0);
			this.PartiesVotesPanel.Name = "PartiesVotesPanel";
			this.PartiesVotesPanel.Size = new System.Drawing.Size(843, 450);
			this.PartiesVotesPanel.TabIndex = 2;
			this.PartiesVotesPanel.Visible = false;
			// 
			// PartyGridViev
			// 
			this.PartyGridViev.AllowUserToAddRows = false;
			this.PartyGridViev.AllowUserToDeleteRows = false;
			this.PartyGridViev.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PartyGridViev.BorderStyle = System.Windows.Forms.BorderStyle.None;
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.PartyGridViev.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this.PartyGridViev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.PartyGridViev.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
			dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.PartyGridViev.DefaultCellStyle = dataGridViewCellStyle9;
			this.PartyGridViev.GridColor = System.Drawing.SystemColors.Control;
			this.PartyGridViev.Location = new System.Drawing.Point(0, 48);
			this.PartyGridViev.Name = "PartyGridViev";
			this.PartyGridViev.ReadOnly = true;
			this.PartyGridViev.RowTemplate.Height = 50;
			this.PartyGridViev.Size = new System.Drawing.Size(843, 399);
			this.PartyGridViev.TabIndex = 1;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
			this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle7;
			this.dataGridViewTextBoxColumn2.FillWeight = 99.61929F;
			this.dataGridViewTextBoxColumn2.HeaderText = "Party";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(168)))), ((int)(((byte)(200)))));
			dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
			this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
			this.dataGridViewTextBoxColumn3.FillWeight = 50.76142F;
			this.dataGridViewTextBoxColumn3.HeaderText = "Votes";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			// 
			// ChartPanel
			// 
			this.ChartPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ChartPanel.Controls.Add(this.PartiesChart);
			this.ChartPanel.Controls.Add(this.CandidatesChart);
			this.ChartPanel.Location = new System.Drawing.Point(0, 0);
			this.ChartPanel.Name = "ChartPanel";
			this.ChartPanel.Size = new System.Drawing.Size(843, 450);
			this.ChartPanel.TabIndex = 2;
			// 
			// PartiesChart
			// 
			this.PartiesChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PartiesChart.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.PartiesChart.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.TileFlipX;
			this.PartiesChart.BorderlineColor = System.Drawing.Color.DimGray;
			this.PartiesChart.BorderSkin.BackColor = System.Drawing.Color.DarkGray;
			this.PartiesChart.BorderSkin.PageColor = System.Drawing.Color.Transparent;
			chartArea1.Name = "ChartArea1";
			this.PartiesChart.ChartAreas.Add(chartArea1);
			legend1.Alignment = System.Drawing.StringAlignment.Far;
			legend1.BackColor = System.Drawing.Color.Transparent;
			legend1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
			legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
			legend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			legend1.IsTextAutoFit = false;
			legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
			legend1.Name = "Legend1";
			legend1.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Tall;
			this.PartiesChart.Legends.Add(legend1);
			this.PartiesChart.Location = new System.Drawing.Point(470, 88);
			this.PartiesChart.Name = "PartiesChart";
			this.PartiesChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Chocolate;
			series1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalRight;
			series1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Bottom;
			series1.BorderColor = System.Drawing.Color.Black;
			series1.ChartArea = "ChartArea1";
			series1.Color = System.Drawing.Color.SeaGreen;
			series1.LabelBackColor = System.Drawing.Color.Transparent;
			series1.LabelForeColor = System.Drawing.Color.Transparent;
			series1.Legend = "Legend1";
			series1.Name = "Votes";
			series1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Chocolate;
			series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.UInt32;
			this.PartiesChart.Series.Add(series1);
			this.PartiesChart.Size = new System.Drawing.Size(362, 346);
			this.PartiesChart.TabIndex = 1;
			this.PartiesChart.Text = "chart2";
			// 
			// CandidatesChart
			// 
			this.CandidatesChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.CandidatesChart.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.CandidatesChart.BorderlineColor = System.Drawing.Color.Transparent;
			this.CandidatesChart.BorderlineWidth = 0;
			this.CandidatesChart.BorderSkin.BackColor = System.Drawing.Color.Transparent;
			this.CandidatesChart.BorderSkin.PageColor = System.Drawing.Color.Transparent;
			chartArea2.Name = "ChartArea1";
			this.CandidatesChart.ChartAreas.Add(chartArea2);
			legend2.Alignment = System.Drawing.StringAlignment.Far;
			legend2.BackColor = System.Drawing.Color.Transparent;
			legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
			legend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			legend2.IsTextAutoFit = false;
			legend2.Name = "Legend1";
			legend2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.CandidatesChart.Legends.Add(legend2);
			this.CandidatesChart.Location = new System.Drawing.Point(7, 88);
			this.CandidatesChart.Name = "CandidatesChart";
			this.CandidatesChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
			series2.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.LeftRight;
			series2.BorderColor = System.Drawing.Color.Black;
			series2.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
			series2.ChartArea = "ChartArea1";
			series2.Color = System.Drawing.Color.MediumSeaGreen;
			series2.LabelBackColor = System.Drawing.Color.Transparent;
			series2.LabelBorderColor = System.Drawing.Color.Transparent;
			series2.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
			series2.Legend = "Legend1";
			series2.MarkerBorderColor = System.Drawing.Color.Transparent;
			series2.MarkerColor = System.Drawing.Color.Transparent;
			series2.Name = "Votes";
			series2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
			series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Single;
			series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
			this.CandidatesChart.Series.Add(series2);
			this.CandidatesChart.Size = new System.Drawing.Size(457, 346);
			this.CandidatesChart.TabIndex = 0;
			// 
			// StatisticsPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.TopLabel);
			this.Controls.Add(this.PartiesButton);
			this.Controls.Add(this.CandidatesButton);
			this.Controls.Add(this.ChartButton);
			this.Controls.Add(this.LogoutButton);
			this.Controls.Add(this.CandidatesVotesPanel);
			this.Controls.Add(this.PartiesVotesPanel);
			this.Controls.Add(this.ChartPanel);
			this.Name = "StatisticsPanel";
			this.Size = new System.Drawing.Size(843, 559);
			this.Load += new System.EventHandler(this.StatisticsPanel_Load);
			this.CandidatesVotesPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.CandidateGridViev)).EndInit();
			this.PartiesVotesPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PartyGridViev)).EndInit();
			this.ChartPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PartiesChart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CandidatesChart)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel CandidatesVotesPanel;
		private System.Windows.Forms.Label TopLabel;
		private System.Windows.Forms.DataGridView CandidateGridViev;
		private System.Windows.Forms.Button ChartButton;
		private System.Windows.Forms.Button LogoutButton;
		private System.Windows.Forms.DataGridViewTextBoxColumn CandidateName;
		private System.Windows.Forms.DataGridViewTextBoxColumn Party;
		private System.Windows.Forms.DataGridViewTextBoxColumn Votes;
		private System.Windows.Forms.Button CandidatesButton;
		private System.Windows.Forms.Button PartiesButton;
		private System.Windows.Forms.Label InvalidVotesLabel;
		private System.Windows.Forms.Panel PartiesVotesPanel;
		private System.Windows.Forms.DataGridView PartyGridViev;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.Panel ChartPanel;
		public System.Windows.Forms.DataVisualization.Charting.Chart PartiesChart;
		public System.Windows.Forms.DataVisualization.Charting.Chart CandidatesChart;
	}
}
