﻿namespace VotingCalculatorFP.Viev
{
	partial class CandidateControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NameLabel = new System.Windows.Forms.Label();
			this.CheckBox = new System.Windows.Forms.CheckBox();
			this.PartyLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// NameLabel
			// 
			this.NameLabel.AutoSize = true;
			this.NameLabel.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.NameLabel.ForeColor = System.Drawing.Color.White;
			this.NameLabel.Location = new System.Drawing.Point(64, 10);
			this.NameLabel.Name = "NameLabel";
			this.NameLabel.Size = new System.Drawing.Size(59, 24);
			this.NameLabel.TabIndex = 0;
			this.NameLabel.Text = "Name";
			// 
			// CheckBox
			// 
			this.CheckBox.AutoSize = true;
			this.CheckBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.CheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.CheckBox.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.CheckBox.Location = new System.Drawing.Point(25, 16);
			this.CheckBox.Name = "CheckBox";
			this.CheckBox.Size = new System.Drawing.Size(15, 14);
			this.CheckBox.TabIndex = 1;
			this.CheckBox.UseVisualStyleBackColor = false;
			// 
			// PartyLabel
			// 
			this.PartyLabel.AutoSize = true;
			this.PartyLabel.Font = new System.Drawing.Font("Calibri", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PartyLabel.ForeColor = System.Drawing.Color.White;
			this.PartyLabel.Location = new System.Drawing.Point(422, 10);
			this.PartyLabel.Name = "PartyLabel";
			this.PartyLabel.Size = new System.Drawing.Size(53, 24);
			this.PartyLabel.TabIndex = 2;
			this.PartyLabel.Text = "Party";
			// 
			// CandidateControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DimGray;
			this.Controls.Add(this.PartyLabel);
			this.Controls.Add(this.CheckBox);
			this.Controls.Add(this.NameLabel);
			this.CandidateName = "CandidateControl";
			this.Size = new System.Drawing.Size(713, 45);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label NameLabel;
		private System.Windows.Forms.CheckBox CheckBox;
		private System.Windows.Forms.Label PartyLabel;
	}
}
