﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCalculatorFP.Viev
{
	interface ILoginPanel
	{
		#region Properties
		string UsersName { get; }
		string UsersSurname { get; }
		string UsersPESEL { get; }
		#endregion

		#region Events
		event Func<string, bool> IsPeselProper;
		event Func<string, bool> IsOver18;
		event Func<string, string, string, bool> CanVote;
		event Action<string, string, string> CreateVoter;
		event Action GoToVotePanel;
		#endregion

	}
}
