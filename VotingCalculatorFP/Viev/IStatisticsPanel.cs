﻿using System;
using System.Collections.Generic;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Viev
{
	interface IStatisticsPanel
	{
		event Action GoBackToHomePanel;
		event Func<List<Candidate>> LoadCandidatesList;
		event Func<uint> CountInvalidVotes;
		event Func<List<Party>> LoadParties;
	}
}
