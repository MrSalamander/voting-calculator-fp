﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using VotingCalculatorFP.Model.DataStructs;
using System.ComponentModel;

namespace VotingCalculatorFP.Viev
{
	public partial class VotingPanel : UserControl,IVotingPanel
	{
		#region Properties
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public List<CandidateControl> ControlList { get; set; }
		#endregion

		#region Events
		public event Func<List<Candidate>> LoadCandidatesList;
		public event Action<bool, Candidate> Vote;
		public event Action GoToSummaryPanel;
		public event Action GoBackToHomePanel;
		#endregion

		#region Methods

		public void ClearPanel()
		{
			ScrollPanel.Controls.Clear();
		}

		public async void GenerateCandidatesControls()
		{
			ControlList.Clear();
			FillCandidateList();

			if (_candidates == null) return;
			foreach(Candidate person in _candidates)
			{
				GenerateControll(person);
			}
		}

		private void FillCandidateList()
		{
			if (LoadCandidatesList != null)
				this._candidates = LoadCandidatesList.Invoke();
		}

		private void GenerateControll(Candidate person)
		{
			var tmp = new CandidateControl();
			InitializeControll(person, tmp);	
			InstallControll(tmp);	
		}

		private void InitializeControll(Candidate person, CandidateControl control)
		{
			control.Location = new Point(10, this.ScrollPanel.Controls.Count * 55 + 15);
			control.CandidateName = person.Name;
			control.Party = person.Party;
		}

		private void InstallControll(CandidateControl controll)
		{
			this.ScrollPanel.Controls.Add(controll);
			ControlList.Add(controll);
		}

		private bool IsVoteValid()
		{
			int _amount = 0;
			if (ControlList == null) return false;
			foreach(CandidateControl control in ControlList)
			{
				if (control.Checked == true) _amount++;
			}

			if (_amount != 1) return false;
			else return true;
		}

		private Candidate getChosenCandidate()
		{
			if (ControlList == null) return null;
			foreach (CandidateControl control in ControlList)
			{
				if (control.Checked == true) return new Candidate(control.CandidateName, control.Party);
			}
			return null;
		}
		#endregion


		#region EventHandling
		#endregion

		#region Constructor
		public VotingPanel()
		{
			InitializeComponent();
			ControlList = new List<CandidateControl>();
		}

		#endregion

		#region Fields
		private List<Candidate> _candidates = new List<Candidate>();

		#endregion

		private void VoteButton_Click(object sender, EventArgs e)
		{
			var confirmResult = MessageBox.Show(
				"Do you confirm your vote? " +
				"\n You wont be able to change decysion.",
				"Confirm the selection",
				 MessageBoxButtons.YesNo);
			if (confirmResult == DialogResult.Yes)
			{
				bool _voteIsValid = IsVoteValid();
				if (Vote != null)
				{
					try
					{
						if (_voteIsValid)
						{
							Candidate candidate = getChosenCandidate();
							if (Vote != null) Vote.Invoke(true, candidate);
						}
						else Vote.Invoke(false, null);
					}catch(Exception ex)
					{
						ShowException(ex);
					}
				}
				ClearPanel();
				GoToSummaryPanel?.Invoke();
			}
			else
			{
			}
			
		}

		private void ShowException(Exception ex)
		{
			MessageBox.Show(ex.Message);
		}

		private void LogoutButton_Click(object sender, EventArgs e)
		{
			ClearPanel();
			GoBackToHomePanel?.Invoke();
		}

	}
}
