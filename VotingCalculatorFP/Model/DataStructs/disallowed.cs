﻿using System;
using System.Collections.Generic;

namespace VotingCalculatorFP.Model.DataStructs
{
	[Serializable]
	public class Disallowed
	{	
		public string PublicationDate { get; set; }
		public List<person> Person { get; set; }
	}
}
