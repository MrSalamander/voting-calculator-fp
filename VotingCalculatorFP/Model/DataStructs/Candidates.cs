﻿using System.Collections.Generic;

namespace VotingCalculatorFP.Model.DataStructs
{
	public class Candidates
	{
		public string PublicationDate { get; set; }
		public List<Candidate> Candidate { get; set; }
	}
}
