﻿namespace VotingCalculatorFP.Model.DataStructs
{
	public class Party
	{
		public string Name { get; set; }
		public uint Votes { get; set; }

		public Party(string Name)
		{
			this.Name = Name;
			Votes = 0;
		}
		public Party()
		{

		}
	}
}
