﻿using System;

namespace VotingCalculatorFP.Model.DataStructs
{
	[Serializable]
	public class DisallowedObj
	{
		public Disallowed Disallowed { get; set; }
	}
}
