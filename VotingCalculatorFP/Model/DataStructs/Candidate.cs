﻿namespace VotingCalculatorFP.Model.DataStructs
{
	public class Candidate
	{
		public string Name { get; set; }
		public string Party { get; set; }
		public uint Votes { get; set; }

		public Candidate()
		{

		}
		public Candidate(string name, string party)
		{
			Name = name;
			Party = party;
		}
	}
}
