﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Model.DAO
{
	public class VotingUserRepository : IVotingUserRepository
	{
		public static event Action<string> SendMessageToView;

		public List<Candidate> GetCandidatesFromDB(List<Candidate> candidates)
		{
			try {
				List<Candidate> list = candidates;
				string query = $"SELECT SelectedCandidate, COUNT(*) FROM valid_votes GROUP BY SelectedCandidate;";
				var _connector = DatabaseConnector.Instance;
				var _cmd = new MySqlCommand(query, _connector.Connection);
				var _reader = _cmd.ExecuteReader();
				while (_reader.Read())
				{
					list.Find(x => x.Name.Equals(_reader.GetString(0))).Votes = _reader.GetUInt32(1);
				}
				_reader.Close();
				return list;
			} catch(Exception e)
			{
				//log 
				Console.WriteLine(e.Message);
				return null;
			}
		}

		public List<Party> GetPartiesFromDB(List<Party> list)
		{
			string query = $"SELECT CandidateParty, COUNT(*) FROM valid_votes GROUP BY CandidateParty;";
			var _connector = DatabaseConnector.Instance;
			var _cmd = new MySqlCommand(query, _connector.Connection);
			try { 
			var _reader = _cmd.ExecuteReader();
			while (_reader.Read())
			{
				list.Find(x => x.Name.Equals(_reader.GetString(0))).Votes = _reader.GetUInt32(1);
			}
			_reader.Close();
			return list;
			}
			catch (Exception e)
			{
				//log 
				Console.WriteLine(e.Message);
				return null;
			}
		}

		public uint CountInvalidVotes()
		{
			string query = $"SELECT COUNT(*) FROM invalid_votes;";
			var _connector = DatabaseConnector.Instance;
			var _cmd = new MySqlCommand(query, _connector.Connection);
			var _reader = _cmd.ExecuteReader();
			_reader.Read();
			uint amount = _reader.GetUInt32(0);
			_reader.Close();
			return amount;
		}

		public async void InsertAsync(User user)
		{
			SimpleAES crypter = new SimpleAES();
			user.Name = crypter.EncryptToString(user.Name);
			user.Surname = crypter.EncryptToString(user.Surname);
			user.PESEL = crypter.EncryptToString(user.PESEL);

			if (user.ValidVote == true)
				InsertValidVote(user);
			else
				InsertInvalidVote(user);
		}

		private async void InsertInvalidVote(User user)
		{
			try
			{
				string query1 = $"INSERT INTO invalid_votes(PESEL, UserName, UserSurname) " +
					$"values ('{user.PESEL}','{user.Name}','{user.Surname}')";

				var _connector = DatabaseConnector.Instance;
				var _cmd = new MySqlCommand(query1, _connector.Connection);
				_cmd.ExecuteNonQuery();
				_connector.Disconnect();
			}catch(Exception e)
			{
				SendMessageToView?.Invoke("Cannot insert vote do Data Base");
			}
		}

		 private async void InsertValidVote(User user)
		{
			try
			{
				string query1 = $"INSERT INTO valid_votes(PESEL, UserName, UserSurname, SelectedCandidate, CandidateParty) " +
					$"values ('{user.PESEL}','{user.Name}','{user.Surname}','{user.ChosenCandidate.Name}','{user.ChosenCandidate.Party}')";

				var _connector = DatabaseConnector.Instance;
				var _cmd = new MySqlCommand(query1, _connector.Connection);
				_cmd.ExecuteNonQuery();
				_connector.Disconnect();
		}
			catch (Exception e)
			{
				SendMessageToView?.Invoke("Cannot insert vote do Data Base");
	}
}
	}
}
