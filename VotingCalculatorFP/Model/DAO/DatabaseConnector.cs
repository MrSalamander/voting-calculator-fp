﻿using System;
using MySql.Data.MySqlClient;

namespace VotingCalculatorFP.Model.DAO
{
	public sealed class DatabaseConnector
	{
		#region Statics
		private static string DATABASE_SERVER_ADDRESS = "mysql-792761.vipserv.org";
		private static string DATABASE_NAME = "mrsala_FPVoting";
		private static string USER_ID = "mrsala_App";
		private static string USER_PASSWORD = "123456789Fp";
		private static uint   PORT = 3306;

		public static event Action<string> SendMessageToView;
		#endregion

		public static bool SetConnectionString(string address, string name, string id, string password, uint port)
		{
			try
			{
				DATABASE_SERVER_ADDRESS = address;
				DATABASE_NAME = name;
				USER_ID = id;
				USER_PASSWORD = password;
				PORT = port;
				return true;
			}catch(Exception )
			{
				DATABASE_SERVER_ADDRESS = "mysql-792761.vipserv.org";
				DATABASE_NAME = "mrsala_FPVoting";
				USER_ID = "mrsala_App";
				USER_PASSWORD = "123456789Fp";
				PORT = 3306;
				return false;
			}
		}

		public static MySqlBaseConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();

		public static DatabaseConnector Instance
		{
			get
			{
				if (_instance == null) _instance = new DatabaseConnector();
				return _instance;
			}
		}
		
		public MySqlConnection Connection
		{
			get
			{
				if (_connection == null) Connect();
					return _connection;
			}
			set
			{

			}
		}

		public void Connect()
		{
			try
			{
				string con = conn_string.ToString();
				_connection = new MySqlConnection(conn_string.ToString());
				_connection.Open();
			}catch(Exception e)
			{
				SendMessageToView("Cannot Connect to the server!");
			}
		}
		public void Disconnect()
		{
			Connection.Clone();
		}

		
		#region private constructor
		private DatabaseConnector()
		{
			conn_string.Server = DATABASE_SERVER_ADDRESS;
			conn_string.Database = DATABASE_NAME;
			conn_string.UserID = USER_ID;
			conn_string.Password = USER_PASSWORD;
			conn_string.Port = PORT;

		}
		#endregion

		#region Fields
		private static DatabaseConnector _instance;
		private static MySqlConnection _connection = null;
		#endregion
	}
}
