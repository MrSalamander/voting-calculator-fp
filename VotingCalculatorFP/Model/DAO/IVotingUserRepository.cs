﻿using System.Collections.Generic;
using VotingCalculatorFP.Model.DataStructs;


namespace VotingCalculatorFP.Model.DAO
{
	interface IVotingUserRepository
	{
		void InsertAsync(User user);
		uint CountInvalidVotes();
		List<Candidate> GetCandidatesFromDB(List<Candidate> candidates);
		List<Party> GetPartiesFromDB(List<Party> list);
	}
}
