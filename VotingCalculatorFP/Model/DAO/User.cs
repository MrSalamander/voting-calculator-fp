﻿using MySql.Data.MySqlClient;
using System;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Model.DAO
{
	public class User
	{
		#region Properties
		public string Name { get; set; }
		public string Surname { get; set; }
		public string PESEL { get; set; }
		public Candidate ChosenCandidate { get; set; }
		public bool ValidVote { get; set; }
		#endregion

		#region Events
		public static event Action<string> SendMessageToView;
		#endregion


		#region Constructor
		public User(string Name, string Surname, string PESEL)
		{
			this.Name = Name;
			this.Surname = Surname;
			this.PESEL = PESEL;
		}
		#endregion

		public static bool HadAlreadyVoted(string PESEL)
		{
			SimpleAES crypter = new SimpleAES();

			PESEL = crypter.EncryptToString(PESEL);

			int matched_rows = 0;
			string query1 = $"SELECT (SELECT Count(PESEL) FROM valid_votes WHERE PESEL = {PESEL}) + " +
				$"(SELECT Count(PESEL) FROM invalid_votes WHERE PESEL = {PESEL});";
			var _connector = DatabaseConnector.Instance;
			var _cmd = new MySqlCommand(query1, _connector.Connection);
			try
			{
				var _reader = _cmd.ExecuteReader();
				

				while (_reader.Read())
				{
					matched_rows += _reader.GetInt32(0);
					Console.WriteLine(matched_rows);
				}

				_reader.Close();
				_connector.Disconnect();

			}catch(Exception e)
			{
				SendMessageToView?.Invoke("Cannot check if User had already been voting!");
			}
			if (matched_rows != 0) return true;
			return false;
		}

	}
}
