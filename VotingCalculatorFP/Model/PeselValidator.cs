﻿using System;
using System.Text;

namespace VotingCalculatorFP.Model
{
	public class PeselValidator
	{
		#region Checking pesel controll sum
		private static readonly int[] multipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

		public static bool IsPeselProper(string pesel)
		{
			bool IsOK = false;
			try
			{
				if (pesel.Length == 11)
				{
					IsOK = CountSum(pesel).Equals(pesel[10].ToString());
				}
			}
			catch (Exception)
			{
				IsOK = false;
			}
			return IsOK;
		}

		private static string CountSum(string pesel)
		{
			int sum = 0;
			for (int i = 0; i < multipliers.Length; i++)
			{
				sum += multipliers[i] * int.Parse(pesel[i].ToString());
			}

			int reszta = sum % 10;
			return reszta == 0 ? reszta.ToString() : (10 - reszta).ToString();
		}
		#endregion

		#region Checking the age treshold
		public static bool IsOver18(string pesel)
		{
			DateTime BirthDate = BuildBirthDate(pesel);
			int age = CalculateAge(BirthDate);

			if (age >= 18)
				return true;
			else
				return false;
		}

		private static DateTime BuildBirthDate(string pesel)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(getBirthYear(pesel));
			sb.Append("-");
			sb.Append(getBirthMonth(pesel));
			sb.Append("-");
			sb.Append(getBirthDay(pesel));
			return DateTime.ParseExact
				(
					sb.ToString(),
					"yyyy-MM-dd",
					System.Globalization.CultureInfo.InvariantCulture
				);
		}

		private static int CalculateAge(DateTime BirthDate)
		{
			DateTime Now = DateTime.Now;

			int age = Now.Year - BirthDate.Year;

			if (Now.Month < BirthDate.Month || (Now.Month == BirthDate.Month && Now.Day < BirthDate.Day))
				age--;

			return age;
		}

		private static string getBirthYear(string pesel)
		{
			int year;
			int month;
			year = int.Parse(pesel.Substring(0, 2));
			month = int.Parse(pesel.Substring(2, 2));

			if (month > 60 && month < 73)
			{
				year += 1800;
			}
			else if (month > 0 && month < 13)
			{
				year += 1900;
			}
			else if (month > 20 && month < 33)
			{
				year += 2000;
			}
			else if (month > 40 && month < 53)
			{
				year += 2100;
			}
			else if (month > 60 && month < 73)
			{
				year += 2200;
			}
			return year.ToString();
		}

		private static string getBirthMonth(string pesel)
		{
			int month;
			month = int.Parse(pesel.Substring(2, 2));
			if (month > 80 && month < 93)
			{
				month -= 80;
			}
			else if (month > 20 && month < 33)
			{
				month -= 20;
			}
			else if (month > 40 && month < 53)
			{
				month -= 40;
			}
			else if (month > 60 && month < 73)
			{
				month -= 60;
			}
			string result = month.ToString();

			if (result.Length == 1)
				return "0" + result;
			else
				return month.ToString();
		}

		private static string getBirthDay(string pesel)
		{
			int day;
			day = int.Parse(pesel.Substring(4, 2));

			string result = day.ToString();

			if (result.Length == 1)
				return "0" + result;
			else
				return day.ToString();
		}
		#endregion
	}
}
