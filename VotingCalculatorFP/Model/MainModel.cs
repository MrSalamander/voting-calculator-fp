﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using VotingCalculatorFP.Model.DAO;
using VotingCalculatorFP.Model.DataStructs;

namespace VotingCalculatorFP.Model
{
	public class MainModel
	{
		#region Consts
		private static string BLOCKED_USERS_URL = "http://webtask.future-processing.com:8069/blocked";
		private static string CANDIDATES_URL = "http://webtask.future-processing.com:8069/candidates";
		#endregion

		#region Properties
		public DisallowedObj DisallowedObj{ get; set; }
		public CandidatesObj CandidatesObj { get; set; }
		public User User { get; set; }
		#endregion

		#region Constructor
		public MainModel()
		{
			DisallowedObj = new DisallowedObj();
			CandidatesObj = new CandidatesObj();
			LoadBlockedUsers();
			RefreshCandidatesFromURL();
		}
		#endregion


		public bool CanVote(string name, string surname, string pesel)
		{
			if (!PeselValidator.IsPeselProper(pesel))
				return false;


			if (UserIsBlocked(pesel))
				return false;

			if (User.HadAlreadyVoted(pesel))
				return false;

			return true;
		}

		public void CreateUser(string name, string surname, string pesel)
		{
			this.User = new User(name, surname, pesel);
			
		}

		public void Vote(bool validVote, Candidate candidate)
		{
			if(validVote == true)
			{
				User.ChosenCandidate = new Candidate(candidate.Name, candidate.Party);
			}
			User.ValidVote = validVote;

			_votingUserRepository.InsertAsync(User);

		}

		public uint CountInvalidVotes()
		{
			return _votingUserRepository.CountInvalidVotes();
		}

		public List<Candidate> LoadCandidates()
		{
			RefreshCandidatesFromURL();
			return _votingUserRepository.GetCandidatesFromDB(CandidatesObj.Candidates.Candidate);
		}

		public List<Party> LoadPartyList()
		{
			List<Party> parties = new List<Party>();
			foreach(Candidate candidate in CandidatesObj.Candidates.Candidate)
			{
				if (!parties.Any(item=>item.Name == candidate.Party)) parties.Add(new Party(candidate.Party));
			}
			parties = _votingUserRepository.GetPartiesFromDB(parties);
			return parties;
		}

		private bool UserIsBlocked(string pesel)
		{

			foreach (person person in DisallowedObj.Disallowed.Person)
			{
				if (pesel.Equals(person.pesel)) return true;
			}
			return false;
		}

		private void LoadBlockedUsers()
		{
			string serialized_json = @DownloadSerializedJson(BLOCKED_USERS_URL);

			if (!string.IsNullOrEmpty(serialized_json))
			{
				JsonSerializer serializer = new JsonSerializer();
				DisallowedObj = JsonConvert.DeserializeObject<DisallowedObj>(serialized_json);
			}
		}

		private void RefreshCandidatesFromURL()
		{
			string serialized_json = @DownloadSerializedJson(CANDIDATES_URL);

			if (!string.IsNullOrEmpty(serialized_json))
			{
				JsonSerializer serializer = new JsonSerializer();
				CandidatesObj = JsonConvert.DeserializeObject<CandidatesObj>(serialized_json);
			}
		}

		private string DownloadSerializedJson(string url)
		{
			using (var w = new WebClient() { Encoding = Encoding.UTF8 })
			{
				string json_serialized_data = string.Empty;
				try
				{
					json_serialized_data = w.DownloadString(url);
				}
				catch (Exception e)
				{
				}
				return json_serialized_data;
			}
		}

		#region Fields
		DAO.VotingUserRepository _votingUserRepository = new VotingUserRepository();
		#endregion
	}
}
